// Test d'exemple par défaut :

describe('The Traffic web site home page', () => {
    it('successfully loads', () => {
        cy.visit('/')
    })
})

describe('The Traffic web site configuration page', () => {
    it('successfully loads configuration', () => {
        cy.get('a[href="#configuration"]').click();
    })
})

describe('test 28 segments', () => {
    it('successfully loads configuration', () => {
        cy.get('a[href="#configuration"]').click();
        cy.get('form[data-kind="segment"]').should('have.length', 28); // on verifie les 28 segments
    })
})

describe('test 0 vehicles', () => {
    it('successfully 0 vehicles', () => {
        cy.get('a[href="#configuration"]').click();
        cy.contains("No vehicle available") // aucun vehicule

    })
})
// On test
describe('test 4', () => {
    it('successfully Story 4', () => {
        cy.get('a[href="#configuration"]').click();
        cy.get(":nth-child(5) > :nth-child(2) > .form-control").clear().type("30"); // on modifie la valeur du segment
        cy.get('#segment-5 > .btn').click();
        cy.get('.modal-header').contains("Update information"); // on verifie que la petit fenêtre s'ouvre
        cy.get('.modal-footer > .btn').click(); // on la ferme
        cy.get('.modal-header').should('not.exist');  //on verifie qu'elle est fermée
        cy.contains("No vehicle available"); // on verif qu'il y a aucun vehicule

        cy.request('GET', 'http://127.0.0.1:4567/elements').as('elements')
        // on verifie la valeur du segment 5
        cy.get('@elements').should((response) => {
            expect(response.body['segments'][4]).to.have.property('speed', 30)
        })

    })
})

describe('test 5', () => {
    it('successfully Story 5', () => {
        cy.get('a[href="#configuration"]').click();
        // on modifie le rond point
        cy.get(':nth-child(3) > .card-body > form > :nth-child(2) > .form-control').clear().type("4");
        cy.get(':nth-child(3) > .card-body > form > :nth-child(3) > .form-control').clear().type("15");
        cy.get(':nth-child(3) > .card-body > form > .btn').click();

        cy.get('.modal-footer > .btn').click();
        cy.reload(); // on actualise la page
        cy.get('a[href="#configuration"]').click();
        // on verifie côté client que les valeurs sont bien changées
        cy.get(':nth-child(3) > .card-body > form > :nth-child(2) > .form-control').should('have.value',4)
        cy.get(':nth-child(3) > .card-body > form > :nth-child(3) > .form-control').should('have.value',15)

        // on vérifie aussi côté serveur
        cy.request('GET', 'http://127.0.0.1:4567/elements').as('elements')
        cy.get('@elements').should((response) => {
            expect(response.body['crossroads'][2]).to.have.property('type', "Roundabout")
            expect(response.body['crossroads'][2]).to.have.property('capacity', 4)
            expect(response.body['crossroads'][2]).to.have.property('duration', 15)
        })
    })
})

describe('test 6', () => {
    it('successfully Story 6', () => {
        // on modifie le feu 29
        cy.get('a[href="#configuration"]').click();
        cy.get(':nth-child(1) > .card-body > form > :nth-child(2) > .form-control').clear().type("4")
        cy.get(':nth-child(1) > .card-body > form > :nth-child(3) > .form-control').clear().type("40")
        cy.get(':nth-child(1) > .card-body > form > :nth-child(4) > .form-control').clear().type("8")
        cy.get(':nth-child(1) > .card-body > form > .btn').click()

        cy.get('.modal-footer > .btn').click();
        cy.reload();
        cy.get('a[href="#configuration"]').click();

        // on verifie côté client les valeur du feu
        cy.get(':nth-child(1) > .card-body > form > :nth-child(2) > .form-control').should("have.value",4)
        cy.get(':nth-child(1) > .card-body > form > :nth-child(3) > .form-control').should("have.value",40)
        cy.get(':nth-child(1) > .card-body > form > :nth-child(4) > .form-control').should("have.value",8)

        // on verifie aussi côté serveur
        cy.request('GET', 'http://127.0.0.1:4567/elements').as('elements')
        cy.get('@elements').should((response) => {
            expect(response.body['crossroads'][0]).to.have.property('greenDuration', 40)
            expect(response.body['crossroads'][0]).to.have.property('orangeDuration', 4)
            expect(response.body['crossroads'][0]).to.have.property('nextPassageDuration', 8)
        })
    })
})

describe('test 7', () => {
    it('successfully Story 7', () => {
        cy.get('a[href="#configuration"]').click();

        ////// AJOUT DES VOITURES ////////////////////////
        cy.get('form > :nth-child(1) > .form-control').clear().type("5")
                cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type("26")
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type("50")
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click();

        cy.get('form > :nth-child(1) > .form-control').clear().type("19")
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type("8")
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type("200")
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click();

        cy.get('form > :nth-child(1) > .form-control').clear().type("27")
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type("2")
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type("150")
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click();

        /////////////// TEST VALEUR VOITURES //////////////////////////

        cy.get('.col-md-8 > .table > tbody > :nth-child(1) > :nth-child(2)').should('have.text',"200.0")
        cy.get('.col-md-8 > .table > tbody > :nth-child(2) > :nth-child(2)').should('have.text',"150.0")
        cy.get('.col-md-8 > .table > tbody > :nth-child(3) > :nth-child(2)').should('have.text',"50.0")


        // test nombre voiture égal à 3
        cy.get(':nth-child(4) > .col-md-8 > .table > tbody > tr').should('have.length',3)

        // test des données sur la route vehicles
        cy.request('GET', 'http://127.0.0.1:4567/vehicles').as('vehicles')
        cy.get('@vehicles').should((response) => {
            expect(response.body['200.0'][0]).exist
            expect(response.body['150.0'][0]).exist
            expect(response.body['50.0'][0]).exist
        })

    })
})

describe('test 8', () => {
    it('successfully Story 8', () => {
        cy.get('a[href="#simulation"]').click();

        // on verifie que les voitures sont à l'arrêt
        cy.get(':nth-child(1) > :nth-child(6) > div > .material-icons').contains("block");
        cy.get(':nth-child(2) > :nth-child(6) > div > .material-icons').contains("block");
        cy.get(':nth-child(3) > :nth-child(6) > div > .material-icons').contains("block");

        // on teste que la barre de simu est à 0 et on la lance
        cy.get('.progress-bar').should("have.css","width","0px");
        cy.get('.form-control').clear().type(120);
        cy.get('.btn').click();

        // on verifie que la barre est complète
        cy.get('.progress-bar',{ timeout: 50000 }).should('have.attr','aria-valuenow','100')

        // on verifie que seul le troisième vehicule est en mouvement
        cy.get(':nth-child(1) > :nth-child(6) > div > .material-icons').contains("block")
        cy.get(':nth-child(2) > :nth-child(6) > div > .material-icons').contains("block")
        cy.get(':nth-child(3) > :nth-child(6) > div > .material-icons').contains("play_circle_filled")

    })
})

describe('test 9', () => {
    it('successfully Story 9', () => {
        cy.reload()
        // test nombre voiture égal à 0
        cy.get(':nth-child(4) > .col-md-8 > .table > tbody > tr').should('have.length',0)

        // on teste aussi côté serveur
        cy.request('GET', 'http://127.0.0.1:4567/vehicles').as('vehicles')
        cy.get('@vehicles').should((response) => {
            expect(!Object.keys(response.body).length);
        })

        cy.get('a[href="#configuration"]').click();

        ////// AJOUT DES VOITURES ////////////////////////
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type("26")
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type("50")
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click();

        cy.get('form > :nth-child(1) > .form-control').clear().type("19")
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type("8")
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type("200")
        cy.get('.col-md-4 > form > .btn').click();
        cy.get('.modal-footer > .btn').click();

        cy.get('form > :nth-child(1) > .form-control').clear().type("27")
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type("2")
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type("150")
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click();

        cy.get('a[href="#simulation"]').click();

        // on lance une simu de 500
        cy.get('.form-control').clear().type(500)
        cy.get('.btn').click();
        // on verifie la progress bar à 100%
        cy.get('.progress-bar',{ timeout: 100000 }).should('have.attr','aria-valuenow','100')

        // on verifie que plus aucun vehicule n'est en mouvement
        cy.get(':nth-child(1) > :nth-child(6) > div > .material-icons').contains("block")
        cy.get(':nth-child(2) > :nth-child(6) > div > .material-icons').contains("block")
        cy.get(':nth-child(3) > :nth-child(6) > div > .material-icons').contains("block")


    })
})


describe('test 10', () => {
    it('successfully Story 10', () => {
        cy.reload()
        // test nombre voiture égal à 0
        cy.get(':nth-child(4) > .col-md-8 > .table > tbody > tr').should('have.length',0)
        cy.contains("No vehicle available")
        cy.get('a[href="#configuration"]').click();

        /* c'était pour l'ancien sujet mais je le laisse car j'ai passé du temps à trouver le problème
        //// REMISE A 0 DU FEU ROUGE ////////
        cy.get(':nth-child(1) > .card-body > form > :nth-child(2) > .form-control').clear().type("5")
        cy.get(':nth-child(1) > .card-body > form > :nth-child(3) > .form-control').clear().type("45")
        cy.get(':nth-child(1) > .card-body > form > :nth-child(4) > .form-control').clear().type("10")
        cy.get(':nth-child(1) > .card-body > form > .btn').click()

        cy.get('.modal-footer > .btn').click();
        */

        ////// AJOUT DES VOITURES ////////////////////////
        cy.get('form > :nth-child(1) > .form-control').clear().type("5")
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type("26")
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type("50")
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click();

        cy.get('form > :nth-child(1) > .form-control').clear().type("5")
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type("26")
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type("80")
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click();

        cy.get('form > :nth-child(1) > .form-control').clear().type("5")
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type("26")
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type("80")
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click();

        //// SIMULATION/////
        cy.get('a[href="#simulation"]').click();

        cy.get('.form-control').clear().type("200")
        cy.get('.btn').click()


        //// tests positions des vehicule////////
        cy.get('.progress-bar',{ timeout: 40000 }).should('have.attr','aria-valuenow','100')
        cy.get('tbody > :nth-child(1) > :nth-child(5)').should('have.text',"29")
        cy.get('tbody > :nth-child(2) > :nth-child(5)').should('have.text',"29")
        cy.get('tbody > :nth-child(3) > :nth-child(5)').should('have.text',"17")
    })
})
